import React from 'react';
import {StyleSheet , View, ImageBackground, Text, TouchableOpacity} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

export default function Post({post, onPress}) {

	return (
		<TouchableOpacity activeOpacity={0.7} onPress={onPress}>
			<View style={styles.post}>
				<ImageBackground source={{uri: post.img}} style={styles.image}>
					<LinearGradient
						colors={['transparent', 'rgba(0,0,0, 0.9)']}
						start={{x: 0.5, y: 0.5}}
						end={{x: 0.5, y: 1}}
						style={{
							position: 'absolute',
							left: 0,
							right: 0,
							top: 0,
							height: 200
						}}
					/>
					<View style={styles.date}>
						<Text style={styles.dateText}>
							{new Date(post.date).toLocaleDateString()}
						</Text>
					</View>
				</ImageBackground>
			</View>
		</TouchableOpacity>
	);
}

const styles = StyleSheet.create({
	image: {
		width: '100%',
		height: 200,
		justifyContent: 'flex-end'
	},
	post: {
		marginTop: 15,
		overflow: 'hidden',
		borderRadius: 20,
	},
	date: {
		alignItems: 'flex-end',
		justifyContent: 'center',
		padding: 10,
		paddingRight: 20
	},
	dateText: {
		color: 'white'
	}
});

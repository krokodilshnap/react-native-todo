import React from 'react';
import {FlatList , SafeAreaView , StyleSheet , View} from 'react-native';
import Post from './Post';

export default function PostList({data, onPress}) {
	return (
		<SafeAreaView style={styles.container}>
			<FlatList
				data={data}
				renderItem={({item, index}) => (
					<>
						<Post post={item} onPress={() => onPress(item)}/>
						{index === data.length - 1 && <View style={styles.bottomPadding} />}
					</>
				)}
				keyExtractor={post => post.id.toString()}
			/>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingHorizontal: 20,
	},
	bottomPadding: {
		paddingBottom: 20,
		width: '100%'
	}
});

import React , {useState} from 'react';
import {StyleSheet , View, Button, Image, Alert} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';

export default function PickImage({onPickImg}) {
	async function onPickHandler() {
		const {status} = await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL);
		if (status !== 'granted') {
			Alert.alert('Необходимо разрешение на использование камеры');
		} else {
			await pickImage();
		}
	}

	async function pickImage() {
		let result = await ImagePicker.launchCameraAsync({
			mediaTypes: ImagePicker.MediaTypeOptions.All,
			allowsEditing: false,
			aspect: [16, 9],
			quality: 1,
		});

		if (!result.cancelled) {
			onPickImg(result.uri);
		}

	}

	return (
		<View style={styles.container}>
			<View>
				<Button title="Добавить фото" onPress={onPickHandler}/>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		marginBottom: 20
	}
});

import React from 'react';
import {StyleSheet , View} from 'react-native';
import {HeaderButton} from 'react-navigation-header-buttons';
import { Ionicons } from '@expo/vector-icons';

export default function AppIcon(props) {
	return (
		<HeaderButton
			IconComponent={Ionicons}
			iconSize={24}
			color="blue"
			{...props}/>

	);
}

const styles = StyleSheet.create({});

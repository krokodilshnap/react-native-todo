import React , {useMemo , useRef} from 'react';
import {Platform , StyleSheet , View} from 'react-native';
import AppIcon from './AppIcon';
import {HeaderButtons , Item} from 'react-navigation-header-buttons';
import {THEME} from '../themes/theme';

export default function AppIconButton(props) {
	const {onPress, iconName, color} = props;
	const iconColor = useMemo(() => {
		if (color) return color;
		return Platform.OS === 'ios' ? THEME.MAIN_COLOR : 'white';
	}, [color]);

	return (
		<HeaderButtons HeaderButtonComponent={AppIcon}>
			<Item
				{...props}
				color={iconColor}
				iconName={iconName}
				onPress={onPress} />
		</HeaderButtons>
	);
}

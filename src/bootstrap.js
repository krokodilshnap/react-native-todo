import {loadAsync} from 'expo-font';
import {DB} from './db';

export async function bootstrap() {
	try {
		await DB.init();
		console.log('db connected');
	} catch (e) {
		console.warn(e);
	}

	return await loadAsync({
		'open-sans': require('../assets/fonts/OpenSans-Regular.ttf'),
		'open-sans-bold': require('../assets/fonts/OpenSans-Bold.ttf')
	})
}

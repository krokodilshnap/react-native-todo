import React , {useState} from 'react';
import {StyleSheet, View, Text, TextInput, Image, ScrollView, Button} from 'react-native';
import {createPost} from '../store/actions/post';
import {useDispatch} from 'react-redux';
import PickImage from '../components/PickImage';

export default function CreateScreen({navigation}) {
	const [text, setText] = useState('');
	const [img, setImg] = useState('');
	const dispatch = useDispatch();

	function createPostHandler() {
		const post = {
			img,
			text,
			date: new Date().toJSON(),
			booked: false
		};
		dispatch(createPost(post));
		clearPost();
		navigation.navigate('Main');
	}

	function clearPost() {
		setText('');
		setImg('');
	}

	function selectPhoto(uri) {
		setImg(uri);
	}

	return (
		<ScrollView keyboardShouldPersistTaps="handled">
			<View style={styles.container}>
				<Text style={styles.heading}>Новый пост</Text>
				<TextInput
					style={styles.input}
					placeholder="Описание поста"
					value={text}
					multiline
					onChangeText={setText}
				/>
				<PickImage onPickImg={selectPhoto} />
				{img ? <Image source={{uri: img}} style={styles.img} /> : null}
				<Button disabled={!text} title="Добавить" onPress={createPostHandler} />
			</View>
		</ScrollView>
	);
}

const styles = StyleSheet.create({
	heading: {
		fontFamily: 'open-sans-bold',
		fontSize: 20,
		textAlign: 'center',
		paddingTop: 20,
		marginBottom: 20
	},
	container: {
		paddingHorizontal: 20,
		marginBottom: 20
	},
	input: {
		marginBottom: 20
	},
	img: {
		width: '100%',
		height: 200,
		marginBottom: 20
	},
});

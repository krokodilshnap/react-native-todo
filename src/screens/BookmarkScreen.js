import React from 'react';
import Post from '../components/Post';
import PostList from '../components/PostList';
import {useSelector} from 'react-redux';
import {Text , View, StyleSheet} from 'react-native';

export default function MainScreen({navigation}) {
	const goToPost = (post) => {
		navigation.navigate('Post', {
			id: post.id,
			date: new Date(post.date).toLocaleDateString(),
			booked: post.booked
		})
	};
	const {bookedPosts} = useSelector(state => state.posts);

	if (!bookedPosts.length) {
		return <View style={styles.empty}>
			<Text>Постов пока что нет</Text>
		</View>
	}

	return <PostList data={bookedPosts} onPress={goToPost} />;
}

const styles = StyleSheet.create({
	empty: {
		paddingHorizontal: 20,
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
});

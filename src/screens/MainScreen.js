import React , {useEffect} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import Post from '../components/Post';
import PostList from '../components/PostList';
import {useDispatch , useSelector} from 'react-redux';
import {loadPosts} from '../store/actions/post';

export default function MainScreen({navigation}) {
	const dispatch = useDispatch();

	const goToPost = (post) => {
		navigation.navigate('Post', {
			id: post.id,
			date: new Date(post.date).toLocaleDateString(),
			booked: post.booked
		})
	};

	useEffect(() => {
		dispatch(loadPosts())
	}, [dispatch]);

	const {allPosts} = useSelector(state => state.posts);
	if (!allPosts.length) {
		return <View style={styles.empty}>
			<Text>Постов пока что нет</Text>
		</View>
	}
	return <PostList data={allPosts} onPress={goToPost}/>
}

const styles = StyleSheet.create({
	empty: {
		paddingHorizontal: 20,
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	bottomPadding: {
		paddingBottom: 20,
		width: '100%'
	}
});

import React from 'react';
import {StyleSheet , View, Text} from 'react-native';

export default function AboutScreen() {
	return (
		<View style={styles.container}>
			<Text>Классное приложение, сам делал.</Text>
			<Text>Версия <Text style={styles.version}>1.0.0</Text></Text>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	version: {
		fontFamily: 'open-sans-bold'
	}
});

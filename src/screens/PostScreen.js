import React , {useLayoutEffect , useState} from 'react';
import {StyleSheet , View, Text, Image, ScrollView, Button, Alert} from 'react-native';
import {DATA} from '../data';
import AppIconButton from '../components/AppIconButton';
import {useDispatch , useSelector} from 'react-redux';
import {addToBookedBy , deletePostBy} from '../store/actions/post';

export default function PostScreen({route, navigation}) {
	const post = useSelector(state => state.posts.allPosts.find(post => post.id === route.params.id));
	const dispatch = useDispatch();
	const [booked, setIsBooked] = useState(route.params.booked);

	function addToBooked(id) {
		setIsBooked(prev => !prev);
		dispatch(addToBookedBy(post));
	}

	useLayoutEffect(() => {
		navigation.setOptions({
			headerRight: () => (
				<AppIconButton
					color="white"
					title="Take a photo"
					iconName={booked ? 'ios-star' : 'ios-star-outline'}
					onPress={() => addToBooked(route.params.id)}
				/>
			),
		})
	}, [navigation, booked]);

	function removeHandler(id) {
		console.log(id);
		Alert.alert('Удалить?', 'Вы уверены что хотите удалить пост†?', [
			{
				text: 'Отмена',
				style: 'cancel'
			},
			{
				text: 'Удалить',
				style: 'destructive',
				onPress: () => {
					navigation.goBack();
					navigation.reset({
						index: 1,
						routes: [
							{name: 'Main'},
							{name: 'Bookmarked'}
						]
					});
					dispatch(deletePostBy(id));
				}
			}
		])
	}

	if (!post) {
		return null
	}

	return (
		<View style={styles.wrapper}>
			<Image source={{uri: post.img}} style={styles.image}/>
			<ScrollView style={styles.container}>
				<Text style={styles.text}>{post.text}</Text>
				<Button title="Удалить" onPress={() => removeHandler(post.id)} />
			</ScrollView>
		</View>
	);
};

const styles = StyleSheet.create({
	image: {
		width: '100%',
		height: 200
	},
	wrapper: {
		flex: 1
	},
	text: {
		fontSize: 18,
		lineHeight: 24,
		padding: 20,
		fontFamily: 'open-sans'
	},
	container: {
		flex: 1
	}
});

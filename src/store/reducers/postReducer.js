import {ADD_TO_BOOKED , CREATE_POST , DELETE_POST , LOAD_POSTS} from '../actions/types';

const initialState = {
	allPosts: [],
	bookedPosts: []
};

export default function postReducer(state = initialState, action) {
	switch (action.type) {
		case LOAD_POSTS:
			return {...state, allPosts: action.payload, bookedPosts: action.payload.filter(post => post.booked)};
		case ADD_TO_BOOKED:
			const allPosts = state.allPosts.map(post => {
				if (post.id === action.id) {
					post.booked = !post.booked
				}
				return post;
			});

			return {...state, allPosts, bookedPosts: allPosts.filter(post => post.booked)};
		case DELETE_POST:
			return {
				...state,
				allPosts: state.allPosts.filter(post => post.id !== action.id),
				bookedPosts: state.bookedPosts.filter(post => post.id !== action.id)
			};
		case CREATE_POST:
			return {...state, allPosts: [action.post, ...state.allPosts]};
		default: return state
	}
}

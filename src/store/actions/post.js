import {ADD_TO_BOOKED , CREATE_POST , DELETE_POST , LOAD_POSTS} from './types';
import {DATA} from '../../data';
import {DB} from '../../db';
import * as FileSystem from 'expo-file-system';

export function loadPosts() {
	return async dispatch => {
		try {
			const posts = await DB.getPosts();
			dispatch({
				type: LOAD_POSTS,
				payload: posts
			})
		} catch (e) {
			console.warn(e)
		}
	}
}

export function addToBookedBy(post) {
	return async dispatch => {
		try {
			await DB.toogleBookmarked(post);
			dispatch({
				type: ADD_TO_BOOKED,
				id: post.id
			})
		} catch (e) {
			console.warn(e)
		}
	}
}

export function deletePostBy(id) {
	return async dispatch => {
		try {
			await DB.deletePost(id);
			dispatch({
				type: DELETE_POST,
				id
			})
		} catch (e) {
			console.warn(e)
		}
	}
}

export function createPost(post) {
	return async dispatch => {
		try {
			const fileName = post.img.split('/').pop();
			const newPath = FileSystem.documentDirectory + fileName;

			await FileSystem.moveAsync({
				to: newPath,
				from: post.img
			});

			const newPost = {...post, img: newPath};

			const id = await DB.createPost(newPost);
			console.log(id);
			dispatch({
				type: CREATE_POST,
				post: {...newPost, id}
			})
		} catch (e) {
			console.warn(e);
		}
	}
}

import BookmarkScreen from '../screens/BookmarkScreen';
import AppIconButton from '../components/AppIconButton';
import PostScreen from '../screens/PostScreen';
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {screenOptions} from './screenOptions';
import {postStackOptions} from './stacksOptions';

const Stack = createStackNavigator();

export default function BookmarkedNavigator({navigation}) {
	return (
		<Stack.Navigator
			screenOptions={{
				headerBackTitle: 'Назад',
				...screenOptions
			}}
			initialRouteName="Bookmarked"
		>
			<Stack.Screen
				name="Bookmarked"
				component={BookmarkScreen}
				options={({route}) => ({
					title: 'Избранное',
					headerLeft: () => (
						<AppIconButton title="Take a photo" iconName="ios-menu" onPress={() => navigation.openDrawer()} />
					)
				})}
			/>
			<Stack.Screen
				name="Post"
				component={PostScreen}
				options={postStackOptions}
			/>
		</Stack.Navigator>
	)
}

import React from 'react';
import {Platform} from 'react-native';
import {THEME} from '../themes/theme';
import MainNavigator from './MainNavigator';
import {Ionicons} from '@expo/vector-icons';
import BookmarkedNavigator from './BookmarkedNavigator';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';

const Tab = Platform.OS === 'ios' ? createBottomTabNavigator() :  createMaterialBottomTabNavigator();

const HomeScreenNavigator = props => {
	return (
		<Tab.Navigator
			shifting={Platform.OS === 'android'}
			tabBarOptions={{
				activeTintColor: THEME.MAIN_COLOR
			}}
			barStyle={{
				backgroundColor: Platform.OS === 'android' ? THEME.MAIN_COLOR : 'white'
			}}
		>
			<Tab.Screen
				name="Main"
				component={MainNavigator}
				options={{
					tabBarLabel: 'Лента',
					tabBarIcon: ({color}) => (
						<Ionicons color={color} name="ios-home" size={25}/>
					)
				}}
			/>
			<Tab.Screen
				name="Bookmarked"
				options={{
					tabBarLabel: 'Избранное',
					tabBarIcon: ({color}) => (
						<Ionicons color={color} name="ios-star" size={25}/>
					)
				}}
				component={BookmarkedNavigator} />
		</Tab.Navigator>
	);
};

export default HomeScreenNavigator;

import React from 'react';
import {screenOptions} from './screenOptions';
import AppIconButton from '../components/AppIconButton';
import {createStackNavigator} from '@react-navigation/stack';
import CreateScreen from '../screens/CreateScreen';

const Stack = createStackNavigator();

const CreateNavigator = ({navigation}) => {
	return (
		<Stack.Navigator
			screenOptions={screenOptions}
			initialRouteName="Create"
		>
			<Stack.Screen
				name="Create"
				component={CreateScreen}
				options={{
					title: 'Добавить пост',
					headerLeft: () => (
						<AppIconButton title="Open menu" iconName="ios-menu" onPress={() => navigation.openDrawer()} />
					)
				}}
			/>
		</Stack.Navigator>
	)
};

export default CreateNavigator;

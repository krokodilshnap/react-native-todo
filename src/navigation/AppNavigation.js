import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import AboutScreen from '../screens/AboutScreen';
import CreateScreen from '../screens/CreateScreen';
import {createDrawerNavigator} from '@react-navigation/drawer';
import HomeScreenNavigator from './HomeScreenNavigator';
import AboutNavigator from './AboutNavigator';
import CreateNavigator from './CreateNavigator';
import {THEME} from '../themes/theme';

const Drawer = createDrawerNavigator();

const AppNavigation = props => {
	return (
		<NavigationContainer>
			<Drawer.Navigator
				initialRouteName="Home"
				drawerContentOptions={{
					activeTintColor: THEME.MAIN_COLOR,
					labelStyle: {fontFamily: 'open-sans-bold'}
				}}>
				<Drawer.Screen
					name="Create"
					component={CreateNavigator}
					options={{
						title: 'Новый пост'
					}}
				/>
				<Drawer.Screen
					name="Home"
					component={HomeScreenNavigator}
					options={{
						title: 'Лента'
					}}/>
				<Drawer.Screen
					name="About"
					component={AboutNavigator}
					options={{
						title: 'О приложении'
					}}
				/>
			</Drawer.Navigator>
		</NavigationContainer>
	);
};

export default AppNavigation;

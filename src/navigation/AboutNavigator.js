import React from 'react';
import {screenOptions} from './screenOptions';
import AppIconButton from '../components/AppIconButton';
import {createStackNavigator} from '@react-navigation/stack';
import AboutScreen from '../screens/AboutScreen';

const Stack = createStackNavigator();

const AboutNavigator = ({navigation}) => {
	return (
		<Stack.Navigator
			screenOptions={screenOptions}
			initialRouteName="About"
		>
			<Stack.Screen
				name="About"
				component={AboutScreen}
				options={{
					title: 'О приложении',
					headerLeft: () => (
						<AppIconButton title="Open menu" iconName="ios-menu" onPress={() => navigation.openDrawer()} />
					)
				}}
			/>
		</Stack.Navigator>
	)
};

export default AboutNavigator;

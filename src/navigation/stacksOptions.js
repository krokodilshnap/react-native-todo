import {THEME} from '../themes/theme';
import React from 'react';

export const postStackOptions = ({route}) => {
	return {
		title: 'Пост от ' + route.params.date ,
		headerStyle: {
			backgroundColor: THEME.SECOND_COLOR
		},
		headerTintColor: 'white'
	}
};

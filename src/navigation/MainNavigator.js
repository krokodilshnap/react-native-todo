import MainScreen from '../screens/MainScreen';
import AppIconButton from '../components/AppIconButton';
import PostScreen from '../screens/PostScreen';
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {screenOptions} from './screenOptions';
import {postStackOptions} from './stacksOptions';

const Stack = createStackNavigator();

export default function MainNavigator({navigation}) {
	return (
		<Stack.Navigator
			screenOptions={screenOptions}
			initialRouteName="Main"
		>
			<Stack.Screen
				name="Main"
				component={MainScreen}
				options={{
					title: 'Мой блог',
					headerRight: () => (
						<AppIconButton title="Take a photo" iconName="ios-camera" onPress={() => navigation.navigate('Create')} />
					),
					headerLeft: () => (
						<AppIconButton title="Open menu" iconName="ios-menu" onPress={() => navigation.openDrawer()} />
					)
				}}
			/>
			<Stack.Screen
				name="Post"
				component={PostScreen}
				options={postStackOptions}
			/>
		</Stack.Navigator>
	)
};

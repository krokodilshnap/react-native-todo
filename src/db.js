import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase('post.db');

export class DB {
	static init() {
		return new Promise((resolve, reject) => {
			db.transaction(tx => {
				tx.executeSql('CREATE TABLE IF NOT EXISTS posts (id INTEGER PRIMARY KEY NOT NULL, img TEXT NOT NULL, text TEXT NOT NULL, date TEXT NOT NULL, booked INTEGER)',
					[],
					resolve,
					(_, error) => reject(error)
				)
			})
		})
	}

	static getPosts() {
		return new Promise((resolve, reject) => {
			db.transaction(tx => {
				tx.executeSql('SELECT * FROM posts',
					[],
					(_, data) => resolve(data.rows._array),
					(_, err) => reject(err)
				)
			})
		})
	}

	static createPost({img, text, date, booked}) {
		return new Promise((resolve, reject) => {
			db.transaction(tx => {
				tx.executeSql('INSERT INTO posts (img, text, date, booked) VALUES (?, ?, ?, ?)',
					[img, text, date, booked],
					(_, data) => resolve(data.insertId),
					(_, err) => reject(err)
				)
			})
		})
	}

	static deletePost(id) {
		return new Promise((resolve, reject) => {
			db.transaction(tx => {
				tx.executeSql('DELETE FROM posts WHERE id = ?',
					[id],
					resolve,
					(_, err) => reject(err)
				)
			})
		})
	}

	static toogleBookmarked({id, booked}) {
		return new Promise((resolve, reject) => {
			db.transaction(tx => {
				tx.executeSql('UPDATE posts SET booked = ? WHERE id = ?',
					[!booked, id],
					resolve,
					(_, err) => reject(err)
				)
			})
		})
	}
}

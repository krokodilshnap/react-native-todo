import React , {useState} from 'react';
import { StyleSheet} from 'react-native';
import {AppLoading} from 'expo';
import {bootstrap} from './src/bootstrap';
import AppNavigation from './src/navigation/AppNavigation';
import {Provider} from 'react-redux';
import {store} from './src/store';


export default function App() {
  const [isReady, setIsReady] = useState(false);


  if (!isReady) {
    return <AppLoading
        startAsync={bootstrap}
        onError={console.warn}
        onFinish={() => setIsReady(true)} />
  }

  return (
      <Provider store={store}>
        <AppNavigation />
      </Provider>
  )

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontFamily: 'open-sans-bold'
  }
});
